////
Copyright 2021 Shikhar Vashistha

Distributed under the Boost Software License, Version 1.0.
(http://www.boost.org/LICENSE_1_0.txt)
////
# Release Notes
:toc: left
:toclevels: 2
:idprefix:
:listing-caption: Code Example
:docinfo: private-footer
:source-highlighter: rouge
:source-language: c++

:leveloffset: +1

include::release_notes.adoc[]


:leveloffset: -1